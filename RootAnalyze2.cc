//Author: Sarah Emilia Andersson 
//nr: +45 42942003 
//mail: sarah.emilia.andersson@nbi.ku.dk 
//home: Niels Bohr Institute - DK Denmark 
#include <iostream>
#include <map>
#include <TCanvas.h>
#include <TChain.h>
#include <TFile.h>
#include <TVectorD.h>
#include <TProfile.h>
#include <TH2.h>
#include <TH1.h>
#include <TParticle.h>
#include <map>
#include <TDatabasePDG.h>
#include <TList.h>
#include <TClonesArray.h>
#include <fstream>
#include <vector>
#include <TArrayD.h>
#include <math.h>
#include <string.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TF1.h>
#include <TMath.h>

using namespace o2::steer;
using namespace o2;
using namespace o2::mcutils;

Long64_t ievent = 0;
int counter_transported = 0; 
int phC_counter = 0; 

using Vec = vector<o2::MCTrack>;



//Make chain of root files (might be relevant)
TChain* MakeChain(){
    TChain* chain = new TChain("o2sim");

    //chain->Add("filtered_Kine.root");

    chain->Add("filtered_Kine.root");

    chain->ResetBranchAddresses();

    return chain;
}


//Analyse entries in root file 
void AnalyzeChain(TChain* chain){

    //Create histograms to store information 

    TH1D* PDG = new TH1D("PDG","PDG primary and transported",900,-300,300);
    PDG->SetXTitle("PDG");
    PDG->SetYTitle("Entries");

    TH1D* PDGall = new TH1D("PDGall","PDG All",900,-300,300);
    PDGall->SetXTitle("PDG");
    PDGall->SetYTitle("Entries");


    //Define type of entries and set branch adress (needs to be pointer for some reason). 

    // Vec* particles = 0; 
    
    vector<o2::MCTrack>* particles = new vector<o2::MCTrack>; 
    

    chain->SetBranchAddress("MCTrack", &particles);

  
    while (chain->GetEntry(ievent++) > 0) {

    
    for (auto P : *particles) {


      auto mother = MCTrackNavigator::getMother(P, *particles);

      if (P.GetPdgCode() == 22) {

      auto first_D = MCTrackNavigator::getDaughter0(P, *particles);

      auto second_D = MCTrackNavigator::getDaughter1(P, *particles);

      if(first_D && second_D) {

        if (abs(first_D->GetPdgCode())==11 && abs(second_D->GetPdgCode())==11) {

          std::cout << "Check it is a gamma:" << P.GetPdgCode() << "get first daugther" << first_D->GetPdgCode() << 
          "get second daughter" << second_D->GetPdgCode() << std::endl; 

          phC_counter += 1; 

          std::cout << "get event" << chain->GetEntry(ievent) << std::endl; 



        }


      }

      }
      



      if (P.isPrimary() == 1) {

      PDGall->Fill(P.GetPdgCode());
      counter_transported += 1;
       

      }
      

      if (P.isPrimary() == 1 && P.isTransported() == 1) { 


        PDG->Fill(P.GetPdgCode());
        //std::cout << "mother ID" << P.getMotherTrackId() << std::endl; 
        //counter_transported += 1;

        
        
      }

        
        
    //   }
      

     }


//End loop over entries in .root file 
      
     }

     std::cout << "Amount transported" << counter_transported << std::endl;  
     std::cout << "Amount of photon conversions" << phC_counter << std::endl; 


     TCanvas* c = new TCanvas("c","PDG");
        c->Divide(2,1);

        c->cd(1);

        gPad->SetLogy();
        
        PDG->Draw("HIST");

        c->cd(2); 

        gPad->SetLogy();

        PDGall->Draw("HIST");
     }





// call main function 
void RootAnalyze2(){

    TChain* chain = MakeChain();

    //std::cout << "chain" << chain << std::endl; 

    AnalyzeChain(chain); 

    //chain->ResetBranchAddress("MCTrack");
    

}

