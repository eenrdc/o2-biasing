//Author: Sarah Emilia Andersson 
//nr: +45 42942003 
//mail: sarah.emilia.andersson@nbi.ku.dk 
//home: Niels Bohr Institute - DK Denmark 
#include <iostream>
#include <map>
#include <TCanvas.h>
#include <TChain.h>
#include <TFile.h>
#include <TVectorD.h>
#include <TProfile.h>
#include <TH2.h>
#include <TH1.h>
#include <TParticle.h>
#include <map>
#include <TDatabasePDG.h>
#include <TList.h>
#include <TClonesArray.h>
#include <fstream>
#include <vector>
#include <TArrayD.h>
#include <math.h>
#include <string.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TF1.h>
#include <TMath.h>

using namespace o2::steer;
using namespace o2;
using namespace o2::mcutils;

Long64_t ievent = 0;
int counter_transported = 0; 
int phC_pi0_counter = 0; 

using Vec = vector<o2::MCTrack>;



//Make chain of root files (might be relevant)
TChain* MakeChain(){
    TChain* chain = new TChain("o2sim");

    //chain->Add("filtered_Kine.root");

    chain->Add("filtered_Kine.root");

    chain->ResetBranchAddresses();

    return chain;
}

bool has_gamma_converted(o2::MCTrack& gamma, vector<o2::MCTrack>& par)
{
  if (gamma.GetPdgCode() != 22) return false; // not a photon
  if (gamma.isPrimary() != 1) return false; 
  //if (gamma.GetNDaughters() != 2) return false; // did not produce 2 particles

  auto d1 = MCTrackNavigator::getDaughter0(gamma, par);

  auto d2 = MCTrackNavigator::getDaughter1(gamma, par);

  if(!d1) return false; 
  if(!d2) return false; 

  auto mother1 = MCTrackNavigator::getMother(*d1, par);

  auto mother2 = MCTrackNavigator::getMother(*d2, par);
  
  if(!mother1) return false; 
  if(!mother2) return false; 

  if (abs(d1->GetPdgCode()) != 11 || abs(d2->GetPdgCode()) != 11) return false; // not e+e- pair;

  std::cout << "mother1" << mother1 << "mother2" << mother2 << std::endl; 
  
  //if (d1->R() > 100. || d2->R() > 100.) return false; // both at R < 100 cm
  return true;
};


bool has_pi0_converted(o2::MCTrack* pi0, vector<o2::MCTrack>& part)
{  
  if (pi0->GetPdgCode() != 111) return false;
  if (std::fabs(pi0->GetEta()) > 1.0) return false;
  //if (pi0.GetNDaughters() != 2) return false;
  
  auto& da1 = MCTrackNavigator::getDaughter0(*pi0, part);
  auto& da2 = MCTrackNavigator::getDaughter1(*pi0, part);

  if(!da1) return false; 
  if(!da2) return false; 

  return ( has_gamma_converted(da1, part) && has_gamma_converted(da2, part) );

};

//Analyse entries in root file 
void AnalyzeChain(TChain* chain){

    //Create histograms to store information 

    TH1D* PDG = new TH1D("PDG","PDG primary and transported",900,-300,300);
    PDG->SetXTitle("PDG");
    PDG->SetYTitle("Entries");

    TH1D* PDGall = new TH1D("PDGall","PDG All",900,-300,300);
    PDGall->SetXTitle("PDG");
    PDGall->SetYTitle("Entries");


    //Define type of entries and set branch adress (needs to be pointer for some reason). 

    // Vec* particles = 0; 
    
    vector<o2::MCTrack>* particles = new vector<o2::MCTrack>; 
    

    chain->SetBranchAddress("MCTrack", &particles);

    int event_counter = 0; 
    int phC_counter = 0; 
  
    while (chain->GetEntry(ievent++) > 0) {

      event_counter+=1; 

    for (auto P : *particles) {


      

      //Start gamma -> e+-

      if(has_pi0_converted(P,*particles)) {

        phC_counter +=1; 


      } 



      
      



      if (P.isPrimary() == 1) {

      PDGall->Fill(P.GetPdgCode());
      counter_transported += 1;
       

      }
      

      if (P.isPrimary() == 1 && P.isTransported() == 1) { 


        PDG->Fill(P.GetPdgCode());
        //std::cout << "mother ID" << P.getMotherTrackId() << std::endl; 
        //counter_transported += 1;

        
        
      }

    }

        
        
    //   }
      

     }


//End loop over entries in .root file 
      
     

     std::cout << "Amount transported" << counter_transported << std::endl;  
     std::cout << "Amount of photon conversions" << phC_counter << std::endl; 

     std::cout << "entries" << event_counter << std::endl; 


     TCanvas* c = new TCanvas("c","PDG");
        c->Divide(2,1);

        c->cd(1);

        gPad->SetLogy();
        
        PDG->Draw("HIST");

        c->cd(2); 

        gPad->SetLogy();

        PDGall->Draw("HIST");
}

    





// call main function 
void RootAnalyze6(){

    TChain* chain = MakeChain();

    //std::cout << "chain" << chain << std::endl; 

    AnalyzeChain(chain); 

    //chain->ResetBranchAddress("MCTrack");
    

}


//with and without pi0 
//rare particle eta-prime going to photon,photon 
//check how filter_Kine.root is constructed 
//check aomunt of pi0 in batch files 
