import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import nbi_stat

#ratio of good events per. trial events for 20 trial events per bunch 

ng = np.array([13,6,13,4,10,7,14,8,13,6,11,11,14,8,9,11,10,9,6,12,8,10,9,7])
ntrial = 20

x_rat = np.mean((ng/ntrial))
x_rat_err = np.std(ng/ntrial)/len(ng)

#average time to simulate one full event 

t_all = 204.9 #min, 204.9 min (sys + user) to simulate 1000 events with -j3 and --seed 5 

t_single = t_all/1000 

t_single_real = 197.33/1000

#Function to calculate the full time it takes to simulate #N "good" events. 

def full_time(N_good, t, x_ratio):
    return t*(N_good/x_ratio)

N_wanted = np.linspace(0,300,100)

N_biasing = np.array([50,100,150,200,250,300]) #number of triggered events in the biasing 

biasing_real_time = np.array([3.5,6.2,8.7,10.5,12.0,14.8]) #min might find the error here, but it would take such a long time. 

#Assume 5% uncertainty on biasing time:

biasing_real_time_err = biasing_real_time*0.05

biasing_sysuser_time = np.array([0.7,1.3,1.9,2.3,2.6,3.1]) #min

#Errorband for sys+user time 
fitopt_time = np.array((t_single,x_rat))
fitcov_time = np.array((0.05*t_single_real,x_rat_err))

ey_time = np.sqrt([nbi_stat.propagate_uncertainty(lambda fitopt_time:full_time(yi,*fitopt_time),
                                                     fitopt_time,fitcov_time) for yi in N_wanted]) 

#errorband for real time 
fitopt_time_real = np.array((t_single_real,x_rat))
fitcov_time_real = np.array((0.05*t_single_real,x_rat_err))

ey_time_real = np.sqrt([nbi_stat.propagate_uncertainty(lambda fitopt_time_real:full_time(yi,*fitopt_time_real),
                                                     fitopt_time_real,fitcov_time_real) for yi in N_wanted])

#Error for the points we are interested in N_biasing 

ey_time_real_points = np.sqrt([nbi_stat.propagate_uncertainty(lambda fitopt_time_real:full_time(yi,*fitopt_time_real),
                                                     fitopt_time_real,fitcov_time_real) for yi in N_biasing])


#print(ey_time_real_points)
#function to fit to a straigt line to computed the initialization time 

def fit(x,a,b):
    return a*x+b 

#fitting biased data to straight line 

popt_real, pcov_real = curve_fit(fit, N_biasing, biasing_real_time, [0.05,1.7], biasing_real_time_err)

fitopt = np.array((popt_real[0],popt_real[1]))
fitcov = np.array((pcov_real[0],pcov_real[1])) 


ey_band = np.sqrt([nbi_stat.propagate_uncertainty(lambda fitopt:fit(yi,*fitopt),
                                                      fitopt,fitcov) for yi in N_wanted]) 




#calculate chisquare for fits 

chi2_fit_real = np.sum(((biasing_real_time-fit(N_biasing,*popt_real))**2) / (biasing_real_time_err**2))
NDOF_real = len(biasing_real_time)-2

chiNDOF_real = chi2_fit_real/NDOF_real

plt.figure(1)
plt.plot(N_wanted,full_time(N_wanted,t_single,x_rat),c='b',label=r"$t\frac{N_{good}}{x_{ratio}}$")

fulltime = full_time(N_wanted,t_single,x_rat)

plt.fill_between(N_wanted, fulltime-ey_time, fulltime+ey_time, \
                 label=r"error from $x_{ratio}=\frac{{N_{triggered}}}{{N_{trial}}}$",alpha=0.5,color="lightblue")

#plt.plot(N_good_biasing,time_biasing,'o',c='r',label="time biasing example")
plt.xlabel("#N good events")
plt.ylabel("Total time sys+user [min]")
plt.title("#Trial events = 20, -j=3, --seed 5 and time = sys+user")
plt.legend(loc="upper left",fontsize=12)
plt.savefig("fulltime_sys_and_user.png")
plt.show()



plt.figure(2)
plt.plot(N_wanted,full_time(N_wanted,t_single_real,x_rat),c='g',label=r"$t_{mean}\frac{N_{good}}{x_{ratio}}$")

fulltime_real = full_time(N_wanted,t_single_real,x_rat)

plt.fill_between(N_wanted, fulltime_real-ey_time_real, fulltime+ey_time_real, \
                 label=r"error from $x_{ratio}$ and $t_{mean}$",alpha=0.5,color="lightgreen")

#plt.plot(N_good_biasing,time_biasing,'o',c='r',label="time biasing example")

plt.xlabel(r"$N_{good}$")
plt.ylabel("Total time real [min]")
plt.title(r"Real time using full simulation, -j=3, --seed 5")
plt.legend(loc="upper left", fontsize=12)
plt.savefig("fullsim_time_real.png")
plt.show()



fit_bias = fit(N_wanted,*popt_real)

plt.figure(3)
plt.errorbar(N_biasing,biasing_real_time,yerr=biasing_real_time_err,fmt='o',c='r')
plt.plot(N_wanted,fit(N_wanted,*popt_real),c='r',label=fr"${round(popt_real[0],2)}x+{round(popt_real[1],2)}$ $\quad$ $\dfrac{{\chi^{2}}}{{NDF}}={round(chiNDOF_real,2)}$")
plt.fill_between(N_wanted, fit_bias-ey_band, fit_bias+ey_band,label=r"fit error",alpha=0.5,color="pink")
plt.xlabel(r"$N_{good}$")
plt.ylabel("Time real biasing [min]")
plt.title("Biasing analysis, -j3, --seed 5, N_trial 20")
plt.legend()
plt.savefig("biasing_time.png")
plt.show()



#Plot the relative time difference so time full sim / time biasing example 

#full time for points we are interested in

time_full_points = full_time(N_biasing,t_single_real,x_rat)

#Calculate error on t_realtive=t_full/t_bias 

sigma_time_relative = np.sqrt(((1/biasing_real_time)**2*biasing_real_time_err**2)+((-time_full_points/(biasing_real_time**2))**2*biasing_real_time_err**2))

print("sigma",sigma_time_relative)

plt.figure(4)
plt.errorbar(N_biasing,time_full_points/biasing_real_time,sigma_time_relative,fmt='o',c='magenta',label=r"$\frac{t_{full}}{t_{bias}}$")
plt.xlabel("#N triggered events")
plt.ylabel(r"$\frac{t_{full}}{t_{bias}}\quad$ real [min]",fontsize=16)
plt.title("Relative time of full sim vs. biasing example")
plt.legend(fontsize=16)
plt.savefig("relative_time.png")
plt.show()

print(ng/ntrial)

plt.figure(5)
plt.hist(ng/ntrial,bins=20)
plt.show()

plt.figure(6)
plt.plot(N_wanted,full_time(N_wanted,t_single_real,x_rat)/t_single_real,c='lightblue')
plt.title(r"$N_{tot}$ vs. $N_{good}$")
plt.xlabel(r"$N_{good}$")
plt.ylabel(r"$N_{tot}$")
plt.show()





#new information 
#time for -j3 and 1000 events; user: 204 min + 42.5 sec, sys: 14.5 sec 

#time biasing 50, 20 trial, j3, transport all, tripper phC 

#real	3m34.438s
#user	0m39.470s
#sys	0m2.306s

#100 

#real	6m10.470s
#user	1m13.556s
#sys	0m4.103s

#150 

#real	8m43.893s
#user	1m45.795s
#sys	0m6.127s

#200 

#real	10m35.988s
#user	2m7.699s
#sys	0m7.384s


#250
#real	11m58.930s
#user	2m28.699s
#sys	0m8.405s

#300
#real	14m49.945s
#user	2m57.471s
#sys	0m9.988s

#second time running full sim for n=1000, -j3 and --seed 5 

#real	197m20.180s
#user	591m16.769s
#sys	0m20.651s

