import numpy as np
import matplotlib.pyplot as plt


events_amount = np.array([5,10,15,20]) #N events per simulation for pythia8pp, TGeant4 back end and 8 workers 
time_full = np.array([8.6, 13.6, 21.8, 28.8]) #min sys + user time 

#Average run time for full sim, 100 particles simulated -j 3. 

t_full_mean_j3 = 19.8/100 #min 
t_full_mean = 19.8/3 

def full_time(t, N_good, x_ratio):
    return t*(N_good/x_ratio)


N_good_biasing = np.array([5,10,15,20,25])
time_biasing = np.array([0.5,1.15,1.65,2.0,2.3]) # sys + user time [min] 20 trial events and -j3 

#x_ratio for -j3 and 20 trial events per bunch 

x_ratio_j3_20 = np.array([1/20,1/20,1/20,0/20,1/20,0/20,2/20,0/20,1/20,1/20,1/20,1/20,1/20,2/20,2/20,1/20,1/20,1/20])
x_ratio_j3_20_mean = np.sum(x_ratio_j3_20)/len(x_ratio_j3_20)

#analyze how many photon conversions we get for various amounts of "events" = -n 5,10,15,20

#NphC = np.array([2906,7236,12090,])


#Analyse ratio of good events per amount of trial events x = n_good/n_total 
#ratio, propagate 30 trial events until we trigger on 10 

x_ratio = np.array([1/30,3/30,1/30,0/30,1/30,0/30,1/30,1/30,2/30])

x_ratio_mean = np.sum(x_ratio)/len(x_ratio)

print(x_ratio_mean)

tt = np.array([5/100,4/100])



print(np.sum(tt)/len(tt))

# Amount of 
# N_good = N_tot*x 

N_good = np.linspace(0,30,10)



plt.figure(1)
plt.plot(N_good,full_time(t_full_mean_j3,N_good,x_ratio_j3_20_mean),c='b',label="time full sim t*n_good/x")
plt.plot(N_good_biasing,time_biasing,'o',c='r',label="time biasing example")
plt.xlabel("#N good events")
plt.ylabel("Total time sys+user for -j3 [min]")
plt.title("#Trial events = 20, -j=3, fixed seed and time = sys+user")
plt.legend()
plt.show()


plt.figure(2)
plt.plot(N_good_biasing,full_time(t_full_mean_j3,N_good_biasing,x_ratio_j3_20_mean)/time_biasing,'o',c='y',label="time ratio")
plt.xlabel("#N good events")
plt.ylabel("time_full/time_biasing")
plt.title("Ratio of full time sim over biasing sim")
plt.legend()
plt.show()































