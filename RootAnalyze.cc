//Author: Sarah Emilia Andersson 
//nr: +45 42942003 
//mail: sarah.emilia.andersson@nbi.ku.dk 
//home: Niels Bohr Institute - DK Denmark 
#include <iostream>
#include <map>
#include <TCanvas.h>
#include <TChain.h>
#include <TFile.h>
#include <TVectorD.h>
#include <TProfile.h>
#include <TH2.h>
#include <TH1.h>
#include <TParticle.h>
#include <map>
#include <TDatabasePDG.h>
#include <TList.h>
#include <TClonesArray.h>
#include <fstream>
#include <vector>
#include <TArrayD.h>
#include <math.h>
#include <string.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TF1.h>
#include <TMath.h>

Long64_t ievent = 0;
int counter_transported = 0; 

using Vec = vector<o2::MCTrackT<float>>;

//Make chain of root files (might be relevant)
TChain* MakeChain(){
    TChain* chain = new TChain("o2sim");

    //chain->Add("filtered_Kine.root");

    chain->Add("filtered_Kine.root");

    chain->ResetBranchAddresses();

    return chain;
}


//Analyse entries in root file 
void AnalyzeChain(TChain* chain){

    //Create histograms to store information 

    TH1D* PDG = new TH1D("PDG","PDG primary and transported",900,-300,300);
    PDG->SetXTitle("PDG");
    PDG->SetYTitle("Entries");

    TH1D* PDGall = new TH1D("PDGall","PDG All",900,-300,300);
    PDGall->SetXTitle("PDG");
    PDGall->SetYTitle("Entries");


    //Define type of entries and set branch adress (needs to be pointer for some reason). 

    Vec* particles = 0; 
    
    chain->SetBranchAddress("MCTrack", &particles);

    //std::cout << "particles entries" << particles->isPrimary() << std::endl; 
    


  
    while (chain->GetEntry(ievent++) > 0) {


    
    for (auto P : *particles) {

    //   std::cout << P.Vx() << std::endl;   

    //   std::cout << P.isPrimary() << std::endl; 

    //   std::cout << P.Vy() << std::endl; 

    //   std::cout << P.isTransported() << std::endl; 
    
    //   std::cout << "first mother" << P.GetMother() << std::endl; 



      //Fill 1D hist with PDG numbers  

      if (P.isPrimary() == 1) {

      PDGall->Fill(P.GetPdgCode());
      counter_transported += 1;
       

      }
      

      if (P.isPrimary() == 1 && P.isTransported() == 1) { 


        PDG->Fill(P.GetPdgCode());
        //std::cout << "mother ID" << P.getMotherTrackId() << std::endl; 
        //counter_transported += 1;

        
        
      }
    //   if (P.GetPdgCode() == 11 || P.GetPdgCode() == -11) { 


    //     //PDG->Fill(P.GetPdgCode());
    //     std::cout << "mother ID" << P.getMotherTrackId() << std::endl; 
    //     //counter_transported += 1;

        
        
    //   }
      

     }


//End loop over entries in .root file 
      
     }

     std::cout << "Amount transported" << counter_transported << std::endl;  


     TCanvas* c = new TCanvas("c","PDG");
        c->Divide(2,1);

        c->cd(1);

        gPad->SetLogy();
        
        PDG->Draw("HIST");

        c->cd(2); 

        gPad->SetLogy();

        PDGall->Draw("HIST");
     }





// call main function 
void RootAnalyze(){

    TChain* chain = MakeChain();

    //std::cout << "chain" << chain << std::endl; 

    AnalyzeChain(chain); 

    //chain->ResetBranchAddress("MCTrack");
    

}

