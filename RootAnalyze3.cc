//Author: Sarah Emilia Andersson 
//nr: +45 42942003 
//mail: sarah.emilia.andersson@nbi.ku.dk 
//home: Niels Bohr Institute - DK Denmark 
#include <iostream>
#include <map>
#include <TCanvas.h>
#include <TChain.h>
#include <TFile.h>
#include <TVectorD.h>
#include <TProfile.h>
#include <TH2.h>
#include <TH1.h>
#include <TParticle.h>
#include <map>
#include <TDatabasePDG.h>
#include <TList.h>
#include <TClonesArray.h>
#include <fstream>
#include <vector>
#include <TArrayD.h>
#include <math.h>
#include <string.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TF1.h>
#include <TMath.h>

using namespace o2::steer;
using namespace o2;
using namespace o2::mcutils;

Long64_t ievent = 0;
int counter_transported = 0; 
int phC_pi0_counter = 0; 

using Vec = vector<o2::MCTrack>;



//Make chain of root files (might be relevant)
TChain* MakeChain(){
    TChain* chain = new TChain("o2sim");

    //chain->Add("filtered_Kine.root");

    chain->Add("filtered_Kine.root");

    chain->ResetBranchAddresses();

    return chain;
}


//Analyse entries in root file 
void AnalyzeChain(TChain* chain){

    //Create histograms to store information 

    TH1D* PDG = new TH1D("PDG","PDG primary and transported",900,-300,300);
    PDG->SetXTitle("PDG");
    PDG->SetYTitle("Entries");

    TH1D* PDGall = new TH1D("PDGall","PDG All",900,-300,300);
    PDGall->SetXTitle("PDG");
    PDGall->SetYTitle("Entries");

    TH3D* ZEP = new TH3D("ZEP", "ZEP", 300,-100,50,300,-2,2,300,5,6.26); 
    ZEP->SetTitle("Vertices of conversions, first pi0->gamma,gamma, then gamma->e+e-");
    ZEP->SetXTitle("Z"); 
    ZEP->SetYTitle("eta");
    ZEP->SetZTitle("phi");

    TH3D* XYZ = new TH3D("XYZ", "XYZ", 100,-0.01,0.01,100,-0.01,0.01,100,-0.01,0.01); 
    XYZ->SetTitle("Vertices of conversions in XYZ coordinates");
    XYZ->SetXTitle("X"); 
    XYZ->SetYTitle("Y");
    XYZ->SetZTitle("Z");

    TH2D* XY = new TH2D("XY", "XY", 100,-980,980,100,-980,980);
    XY->SetTitle("XY positions of photon conversions weighted by Pt"); 
    XY->SetXTitle("X"); 
    XY->SetYTitle("Y");

    TH1D* Pt = new TH1D("Pt","Pt",100,0,1); 
    Pt->SetTitle("total Pt of e+ + e- from each conversion"); 
    Pt->SetXTitle("Pt"); 

    TH1D* Pt_photons = new TH1D("Pt_photons","Pt_photons",100,0,1); 
    Pt_photons->SetTitle("Pt distributions of all photons"); 
    Pt_photons->SetXTitle("Pt"); 

    TH1D* phC_per_event_hist = new TH1D("phC_per_event_hist ","phC_per_event_hist ",20,0,20);
    phC_per_event_hist->SetTitle("Amount of pi0->gamma,gamma->e+e-,e+e- per event"); 
    phC_per_event_hist->SetXTitle("full pi0 conversions");

    TH1D* pi0_per_event_hist = new TH1D("pi0_per_event_hist ","pi0_per_event_hist ",20,0,200);
    pi0_per_event_hist->SetTitle("Amount of pi0 per event"); 
    pi0_per_event_hist->SetXTitle("#pi0");

    TH1D* photon_per_event_hist = new TH1D("photon_per_event_hist ","photon_per_event_hist ",30,0,400);
    photon_per_event_hist->SetTitle("Amount of photons per event"); 
    photon_per_event_hist->SetXTitle("#photon");


    //Define type of entries and set branch adress (needs to be pointer for some reason). 

    // Vec* particles = 0; 
    
    vector<o2::MCTrack>* particles = new vector<o2::MCTrack>; 
    

    chain->SetBranchAddress("MCTrack", &particles);

    int event_counter = 0; 
    int phC_counter = 0; 
    int photon_counter = 0; 
    int pi0_counter =0; 

    std::vector<int> pho_conv; 
    std::vector<int> pi0_vec; 
    std::vector<int> photon_vec;
  
    //
    while (chain->GetEntry(ievent++) > 0) {


      event_counter+=1; 

    
      for (auto P : *particles) {

      auto mother = MCTrackNavigator::getMother(P, *particles);

      //Start pi0->gamma,gamma check

      if (P.GetPdgCode() == 22 && P.isPrimary()==1) {
        photon_counter += 1;

      }

      if (P.GetPdgCode() == 111 && P.isPrimary() == 1) {

        pi0_counter +=1; 

      auto first_D = MCTrackNavigator::getDaughter0(P, *particles);

      auto second_D = MCTrackNavigator::getDaughter1(P, *particles);

      if(first_D && second_D) {

        if (abs(first_D->GetPdgCode())==22 && abs(second_D->GetPdgCode())==22) {

          //std::cout << "Check it is a pi0:" << P.GetPdgCode() << "get first daugther" << first_D->GetPdgCode() << 
          //"get second daughter" << second_D->GetPdgCode() << std::endl; 

          //start gamma -> +-e check 

           

          auto first_D_e1 = MCTrackNavigator::getDaughter0(*first_D, *particles);

          auto second_D_e1 = MCTrackNavigator::getDaughter1(*first_D, *particles);


          auto first_D_e2 = MCTrackNavigator::getDaughter0(*second_D, *particles);

          auto second_D_e2 = MCTrackNavigator::getDaughter1(*second_D, *particles);

          

          if(first_D_e1 && second_D_e2) {
            if (abs(first_D_e1->GetPdgCode())==11 && abs(second_D_e1->GetPdgCode())==11 
            && abs(first_D_e2->GetPdgCode())==11 && abs(second_D_e2->GetPdgCode())==11) {

              std::cout << "track ID of first gamma daughter" << P.getFirstDaughterTrackId() << std::endl; 
              std::cout << "track ID of last gamma daughter" << P.getLastDaughterTrackId() << std::endl;

              std::cout << "track ID of mother of first e+" << first_D_e1->getMotherTrackId() << std::endl; 
              std::cout << "track ID of mother of first e-" << second_D_e1->getMotherTrackId() << std::endl; 


              std::cout << "track ID of mother of second e+" << first_D_e2->getMotherTrackId() << std::endl; 
              std::cout << "track ID of mother of second e-" << second_D_e2->getMotherTrackId() << std::endl; 


              // std::cout << "mother posotion first e+" << first_D_e1->getMotherTrackId() << std::endl; 
              // std::cout << "mother position first e-" << second_D_e1->getMotherTrackId() << std::endl; 


              // std::cout << "track ID of mother of second e+" << first_D_e2->getMotherTrackId() << std::endl; 
              // std::cout << "track ID of mother of second e-" << second_D_e2->getMotherTrackId() << std::endl; 

              //std::cout << "Check it is a gamma:" << first_D->GetPdgCode() << "get first daugther" << first_D_e1->GetPdgCode() << 
              //"get second daughter" << second_D_e1->GetPdgCode() << std::endl;

              ZEP->Fill(P.Vz(),P.GetEta(),P.GetPhi(),P.GetPdgCode());

              ZEP->Fill(first_D->Vz(),first_D->GetEta(),first_D->GetPhi(),first_D->GetPdgCode()+40); 
              ZEP->Fill(second_D->Vz(),second_D->GetEta(),second_D->GetPhi(),second_D->GetPdgCode()+40);

              //std::cout << "T" << "   " << first_D->T() << "   " << second_D->T() << "   " << first_D->GetP() << std::endl; 
              std::cout << "eta gamma 1 and gamma 2" << "   " << first_D->GetEta() << "   " << second_D->GetEta() << std::endl;
              std::cout << "phi gamma 1 and gamma 2" << "   " << first_D->GetPhi() << "   " << second_D->GetPhi() << std::endl;
              std::cout << "Z gamma 1 and gamma 2" << "   " << first_D->Vz() << "   " << second_D->Vz() << std::endl;
              // std::cout << "X" << "   " << first_D->Px() << "   " << second_D->Px() << std::endl;
              // std::cout << "Y" << "   " << first_D->Py() << "   " << second_D->Py() << std::endl;

              ZEP->Fill(first_D_e1->Vz(),first_D_e1->GetEta(),first_D_e1->GetPhi(),first_D_e1->GetPdgCode());
              ZEP->Fill(second_D_e1->Vz(),second_D_e1->GetEta(),second_D_e1->GetPhi(),second_D_e1->GetPdgCode());
              ZEP->Fill(first_D_e2->Vz(),first_D_e2->GetEta(),first_D_e2->GetPhi(),first_D_e2->GetPdgCode());
              ZEP->Fill(second_D_e2->Vz(),second_D_e2->GetEta(),second_D_e2->GetPhi(),second_D_e2->GetPdgCode());

              std::cout << "eta for e+ and e- from gamma 1" << "   " << first_D_e1->GetEta() << "   " << second_D_e1->GetEta() << std::endl;
              std::cout << "phi for e+ and e- from gamma 1" << "   " << first_D_e1->GetPhi() << "   " << second_D_e1->GetPhi() << std::endl;
              std::cout << "Z for e+ and e- from gamma 1" << "   " << first_D_e1->Vz() << "   " << second_D_e1->Vz() << std::endl;

              std::cout << "eta for e+ and e- from gamma 2" << "   " << first_D_e2->GetEta() << "   " << second_D_e2->GetEta() << std::endl;
              std::cout << "phi for e+ and e- from gamma 2" << "   " << first_D_e2->GetPhi() << "   " << second_D_e2->GetPhi() << std::endl;
              std::cout << "Z for e+ and e- from gamma 2" << "   " << first_D_e2->Vz() << "   " << second_D_e2->Vz() << std::endl;

              //std::cout << "eta1" << "   " << first_D_e1->GetEta() << std::endl;
              //std::cout << "eta1" << "   " << second_D_e1->GetEta() << std::endl;

              XYZ->Fill(P.Vx(),P.Vy(),P.Vz(),P.GetPdgCode());

              std::cout << "XYZ" << P.Vx() << " " << P.Vy() << " " << P.Vz() << std::endl;  

              XYZ->Fill(first_D->Vx(),first_D->Vy(),first_D->Vz(),first_D->GetPdgCode()+40); 
              XYZ->Fill(second_D->Vx(),second_D->Vy(),second_D->Vz(),second_D->GetPdgCode()+40);


              XYZ->Fill(first_D_e1->Vx(),first_D_e1->Vy(),first_D_e1->Vz(),first_D_e1->GetPdgCode());
              XYZ->Fill(second_D_e1->Vx(),second_D_e1->Vy(),second_D_e1->Vz(),second_D_e1->GetPdgCode());
              XYZ->Fill(first_D_e2->Vx(),first_D_e2->Vy(),first_D_e2->Vz(),first_D_e2->GetPdgCode());
              XYZ->Fill(second_D_e2->Vx(),second_D_e2->Vy(),second_D_e2->Vz(),second_D_e2->GetPdgCode());
              
              //std::cout << "phi1" << "   " << first_D_e1->GetPhi() << "   " << second_D_e1->GetPhi() << std::endl;
              //std::cout << "Z1" << "   " << first_D_e1->Pz() << "   " << second_D_e1->Pz() << std::endl;


              //std::cout << "eta2" << "   " << first_D_e2->GetEta() << "   " << second_D_e2->GetEta() << std::endl;
              //std::cout << "phi2" << "   " << first_D_e2->GetPhi() << "   " << second_D_e2->GetPhi() << std::endl;
              //std::cout << "Z2" << "   " << first_D_e2->Pz() << "   " << second_D_e2->Pz() << std::endl;

              
              phC_pi0_counter += 1;

              


            }
            
          }

           

          //std::cout << "get event" << chain->GetEntry(ievent) << std::endl; 



        }


      }

      }
      



      if (P.isPrimary() == 1) {

      PDGall->Fill(P.GetPdgCode());
      counter_transported += 1;
       

      }
      

      if (P.isPrimary() == 1 && P.isTransported() == 1) { 


        PDG->Fill(P.GetPdgCode());
        //std::cout << "mother ID" << P.getMotherTrackId() << std::endl; 
        //counter_transported += 1;

        
        
      }

        
        
          //   }
      

      }


          //End loop over entries in .root file 
      pho_conv.push_back(phC_pi0_counter);
      pi0_vec.push_back(pi0_counter);
      photon_vec.push_back(photon_counter);
      //std::cout << "amount of photon conversions from pi0 printet per event" << phC_pi0_counter << std::endl; 
     }


     

     std::cout << "Amount transported" << counter_transported << std::endl;  
     std::cout << "Amount of photon conversions from pi0" << phC_pi0_counter << std::endl; 

     std::cout << "entries" << event_counter << std::endl; 

     std::cout << "total amount of photons" << photon_counter << std::endl; 

     //std::vector<int> phC_per_event; 

     for(int i; i<pho_conv.size(); i++) {
      //std::cout << "photon conversions added" << pho_conv[i] << std::endl; 
      int phC_per_event2 = abs(pho_conv[i+1]-pho_conv[i]);
      int pi0_per_event2 = abs(pi0_vec[i+1]-pi0_vec[i]);
      int photon_per_event2 = abs(photon_vec[i+1]-photon_vec[i]);


      //std::cout << "photon conversions in each event" << phC_per_event2 << std::endl; 

      phC_per_event_hist->Fill(phC_per_event2);
      pi0_per_event_hist->Fill(pi0_per_event2);
      photon_per_event_hist->Fill(photon_per_event2);


     }


     TCanvas* c = new TCanvas("c","PDG");
        c->Divide(2,1);

        c->cd(1);

        gPad->SetLogy();
        
        PDG->Draw("HIST");

        c->cd(2); 

        gPad->SetLogy();

        PDGall->Draw("HIST");


      TCanvas* c2 = new TCanvas("c2","ZEP"); 

      c2->Divide(2,1);

      c2->cd(1);

      //gStyle->SetPalette(kNeon); 

      ZEP->SetMarkerStyle(20);

      ZEP->SetMarkerSize(0.5);

      ZEP->Draw("LEGO2Z");

      //c2->cd(2);

      //XYZ->Draw("LEGO2Z");


      TCanvas* c3 = new TCanvas("c3","photon conversions"); 

      c3->Divide(3,1);

      c3->cd(1);

      phC_per_event_hist->Draw("HIST"); 

      c3->cd(2); 

      pi0_per_event_hist->Draw("HIST");

      c3->cd(3);

      photon_per_event_hist->Draw("HIST"); 

      

      //c2->cd(2); 
      //XY->Draw("colz");
     }





// call main function 
void RootAnalyze3(){

    TChain* chain = MakeChain();

    //std::cout << "chain" << chain << std::endl; 

    AnalyzeChain(chain); 

    //chain->ResetBranchAddress("MCTrack");
    

}


//with and without pi0 
//rare particle eta-prime going to photon,photon 
//check how filter_Kine.root is constructed 
//check aomunt of pi0 in batch files 
