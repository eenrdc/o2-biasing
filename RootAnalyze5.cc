//Author: Sarah Emilia Andersson 
//nr: +45 42942003 
//mail: sarah.emilia.andersson@nbi.ku.dk 
//home: Niels Bohr Institute - DK Denmark 
#include <iostream>
#include <map>
#include <TCanvas.h>
#include <TChain.h>
#include <TFile.h>
#include <TVectorD.h>
#include <TProfile.h>
#include <TH2.h>
#include <TH1.h>
#include <TParticle.h>
#include <map>
#include <TDatabasePDG.h>
#include <TList.h>
#include <TClonesArray.h>
#include <fstream>
#include <vector>
#include <TArrayD.h>
#include <math.h>
#include <string.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TF1.h>
#include <TMath.h>

using namespace o2::steer;
using namespace o2;
using namespace o2::mcutils;

Long64_t ievent = 0;
int counter_transported = 0; 
int phC_pi0_counter = 0; 

using Vec = vector<o2::MCTrack>;



//Make chain of root files (might be relevant)
TChain* MakeChain(){
    TChain* chain = new TChain("o2sim");

    //chain->Add("filtered_Kine.root");

    chain->Add("filtered_Kine.root");

    chain->ResetBranchAddresses();

    return chain;
}


//Analyse entries in root file 
void AnalyzeChain(TChain* chain){

    //Create histograms to store information 

    TH1D* PDG = new TH1D("PDG","PDG primary and transported",900,-300,300);
    PDG->SetXTitle("PDG");
    PDG->SetYTitle("Entries");

    TH1D* PDGall = new TH1D("PDGall","PDG All",900,-300,300);
    PDGall->SetXTitle("PDG");
    PDGall->SetYTitle("Entries");

    TH3D* XYZ = new TH3D("XYZ", "XYZ", 100,-900,900,100,-2,2,100,0,6.26); 
    XYZ->SetTitle("Vertices of photon conversions, weigthed by total momentum of the two e+-");
    XYZ->SetXTitle("Z"); 
    XYZ->SetYTitle("eta");
    XYZ->SetZTitle("phi");

    TH2D* XY = new TH2D("XY", "XY", 100,-980,980,100,-980,980);
    XY->SetTitle("XY positions of photon conversions weighted by Pt"); 
    XY->SetXTitle("X"); 
    XY->SetYTitle("Y");

    TH1D* Pt = new TH1D("Pt","Pt",100,0,1); 
    Pt->SetTitle("total Pt of e+ + e- from each conversion"); 
    Pt->SetXTitle("Pt"); 

    TH1D* Pt_photons = new TH1D("Pt_photons","Pt_photons",100,0,1); 
    Pt_photons->SetTitle("Pt distributions of all photons"); 
    Pt_photons->SetXTitle("Pt"); 




    //Define type of entries and set branch adress (needs to be pointer for some reason). 

    // Vec* particles = 0; 
    
    vector<o2::MCTrack>* particles = new vector<o2::MCTrack>; 
    

    chain->SetBranchAddress("MCTrack", &particles);

    int event_counter = 0; 
    int phC_counter = 0; 
    int photon_counter = 0; 
  
    //
    while (chain->GetEntry(ievent++) > 0) {


      event_counter+=1; 


    
    for (auto P : *particles) {

      


      auto mother = MCTrackNavigator::getMother(P, *particles);

      //Start pi0->gamma,gamma check

      if (P.GetPdgCode() == 22 && P.isPrimary() == 1) {

        photon_counter += 1; 

        Pt_photons->Fill(P.GetPt()); 

      auto first_D = MCTrackNavigator::getDaughter0(P, *particles);

      auto second_D = MCTrackNavigator::getDaughter1(P, *particles);

      if(first_D && second_D) {

        if (abs(first_D->GetPdgCode())==11 && abs(second_D->GetPdgCode())==11) {

          //std::cout << "Check it is a gamma:" << P.GetPdgCode() << "get first daugther" << first_D->GetPdgCode() << 
          //"get second daughter" << second_D->GetPdgCode() << std::endl; 

          phC_counter += 1; 

          XYZ->Fill(first_D->Vz(),first_D->GetEta(),(first_D->GetPhi()),(first_D->GetP()+second_D->GetP()));

          XY->Fill(first_D->Vx(),first_D->Vy(),(first_D->GetPt()+second_D->GetPt()));

          Pt->Fill((first_D->GetPt()+second_D->GetPt()));

          std::cout << "T" << "   " << first_D->T() << "   " << second_D->T() << "   " << first_D->GetP() << std::endl; 
          std::cout << "eta" << "   " << first_D->GetEta() << "   " << second_D->GetEta() << "    " << second_D->GetP() << std::endl;
          std::cout << "phi" << "   " << first_D->GetPhi() << "   " << second_D->GetPhi() << std::endl;
          std::cout << "Z" << "   " << first_D->Vz() << "   " << second_D->Vz() << std::endl;


          std::cout << (first_D->GetPt()+second_D->GetPt()) << std::endl; 

          //start gamma -> +-e check 


          //std::cout << "get event" << chain->GetEntry(ievent) << std::endl; 



        }


      }

      }
      



      if (P.isPrimary() == 1) {

      PDGall->Fill(P.GetPdgCode());
      counter_transported += 1;
       

      }
      

      if (P.isPrimary() == 1 && P.isTransported() == 1) { 


        PDG->Fill(P.GetPdgCode());
        //std::cout << "mother ID" << P.getMotherTrackId() << std::endl; 
        //counter_transported += 1;

        
        
      }

        
        
    //   }
      

     }


//End loop over entries in .root file 
      
     }

     std::cout << "Amount transported" << counter_transported << std::endl;  
     std::cout << "Amount of photon conversions" << phC_counter << std::endl; 

     std::cout << "entries" << event_counter << std::endl; 

     std::cout << "total amount of photons" << photon_counter << std::endl; 


     TCanvas* c = new TCanvas("c","PDG");
        c->Divide(2,1);

        c->cd(1);

        gPad->SetLogy();
        
        PDG->Draw("HIST");

        c->cd(2); 

        gPad->SetLogy();

        PDGall->Draw("HIST");


     TCanvas* c2 = new TCanvas("c2","XYZ"); 

      c2->Divide(2,1);

      c2->cd(1);

      XYZ->SetMarkerStyle(20);

      XYZ->SetMarkerSize(0.5);

      XYZ->Draw("LEGO2Z");

      c2->cd(2); 
      XY->Draw("colz");


      TCanvas* c3 = new TCanvas("c3","momentum"); 

      c3->Divide(2,1);

      c3->cd(1);

      Pt->SetMarkerStyle(20);

      Pt->SetMarkerSize(0.5);

      Pt->Draw("HIST");

      c3->cd(2); 

      Pt_photons->Draw("HIST"); 

     }





// call main function 
void RootAnalyze5(){

    TChain* chain = MakeChain();

    //std::cout << "chain" << chain << std::endl; 

    AnalyzeChain(chain); 

    //chain->ResetBranchAddress("MCTrack");
    

}


//with and without pi0 
//rare particle eta-prime going to photon,photon 
//check how filter_Kine.root is constructed 
//check aomunt of pi0 in batch files 
